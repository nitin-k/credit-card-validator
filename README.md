Credit Card Validator
--------------------
This is a gradle-jersey solution.

Available API
--------------
`POST /validate-credit-card`
That accepts a list of credit cards and validates them accordingly.
Here is an example that covers most of the cases mentioned in requirement
document.
**Remove Comments from this JSON before sending a request**
```
[
    {
        // Valid VISA
        "number": "4118578487382760",
        "date": "10/20"
    },
    {
        // Valid MASTERCARD
        "number": "5164827523445762",
        "date": "12/20"
    },
    {
        // Luhn Check failed
        "number": "4118578487382761",
        "date": "10/20"
    },
    {
        // More than 16 digits
        "number": "411857848738276022",
        "date": "10/20"
    },
    {
        // Accepts "xxxx xxxx xxxx xxxx" format
        "number": "4118 5784 8738 2760",
        "date": "10/20"
    },
    {
        // Date in Past
        "number": "4118578487382760",
        "date": "10/17"
    },
    {

        // Accepts "xxxx xxxx xxxx xxxx" format, but not any other format
        "number": "41185 7848 7382760",
        "date": "10/19"
    },
    {
        // Invalid Date format
        "number": "4118 5784 8738 2760",
        "date": "19/19"
    }
]
```
Response
```
[
    {
        "number": "4118578487382760",
        "validationResult": {
            "errorCode": 200,
            "reason": "Credit Card Valid"
        }
    },
    {
        "number": "5164827523445762",
        "validationResult": {
            "errorCode": 200,
            "reason": "Credit Card Valid"
        }
    },
    {
        "number": "4118578487382761",
        "validationResult": {
            "errorCode": 422,
            "reason": "Number failed the Luhn Check"
        }
    },
    {
        "number": "411857848738276022",
        "validationResult": {
            "errorCode": 421,
            "reason": "Invalid Credit Card Format"
        }
    },
    {
        "number": "4118 5784 8738 2760",
        "validationResult": {
            "errorCode": 200,
            "reason": "Credit Card Valid"
        }
    },
    {
        "number": "4118578487382760",
        "validationResult": {
            "errorCode": 423,
            "reason": "Credit Card date is expired"
        }
    },
    {
        "number": "41185 7848 7382760",
        "validationResult": {
            "errorCode": 421,
            "reason": "Invalid Credit Card Format"
        }
    },
    {
        "number": "4118 5784 8738 2760",
        "validationResult": {
            "errorCode": 426,
            "reason": "Text '19/19' could not be parsed: Unable to obtain YearMonth from TemporalAccessor: {MonthOfYear=19, Year=2019},ISO of type java.time.format.Parsed"
        }
    }
]
```

Build
------
`./gradlew build` generates the war in `build/libs` folder of the project. This can be
deployed to any tomcat instance for having this API available


Run API locally Command line
---------------
./gradlew jettyRunWar
API is available at context: `http://localhost:8080/<project-folder-name>`
(POST API is available at `/validate-credit-card`)


Notes/Limitations
------
- Haven't unit tested Any of the Resource, Would have done it if have more
time
- Did not implement any Dependency injection frame work
- Validation of the domain objects are done in the class instead of service
keeping the scalability in picture( It is not true in this case)
- Black listed numbers specified in requirement document does not pass Luhn
Check, so used some valid ones to demonstrate them

