package com.nitin.service;

import com.google.common.collect.Sets;
import com.nitin.domain.InvalidCreditCardNumberException;
import com.nitin.domain.InvalidCreditCardNumberFormatException;
import com.nitin.domain.creditcard.CreditCardNumber;

import java.util.Set;

class BlackListedCreditCardService {
    private Set<CreditCardNumber> blackListedCreditCardNumber;

    BlackListedCreditCardService() throws InvalidCreditCardNumberException, InvalidCreditCardNumberFormatException {
        // the list of black listed number are hard coded
        // This list can be dynamic
        this.blackListedCreditCardNumber =
                Sets.newHashSet(new CreditCardNumber("4798 1176 5878 1703"),
                        new CreditCardNumber("5503 3441 9001 7262"));
    }

    boolean isBlackListed(CreditCardNumber creditCardNumber) {
        return blackListedCreditCardNumber.contains(creditCardNumber);
    }
}
