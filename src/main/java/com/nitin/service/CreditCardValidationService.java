package com.nitin.service;

import com.nitin.api.dto.CreditCardValidationRequest;
import com.nitin.api.dto.CreditCardValidationResult;
import com.nitin.api.dto.ValidationResult;
import com.nitin.domain.CreditCardNotSupportedException;
import com.nitin.domain.CreditCardOutdatedException;
import com.nitin.domain.InvalidCreditCardNumberException;
import com.nitin.domain.InvalidCreditCardNumberFormatException;
import com.nitin.domain.creditcard.CreditCard;
import com.nitin.domain.creditcard.CreditCardNumber;
import com.nitin.domain.creditcard.CreditCardType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.YearMonth;
import java.time.format.DateTimeParseException;

import static com.nitin.domain.CreditCardNotSupportedException.throwInvalidCreditCardType;
import static com.nitin.domain.creditcard.CreditCard.creditCardDateFormatter;

public class CreditCardValidationService {
    private static final Logger logger = LoggerFactory.getLogger(CreditCardValidationService.class);
    public static final String VALID_CREDIT_CARD_MESSAGE = "Credit Card Valid";

    private BlackListedCreditCardService blackListedCreditCardService;

    public CreditCardValidationService() throws InvalidCreditCardNumberException, InvalidCreditCardNumberFormatException {
        blackListedCreditCardService = new BlackListedCreditCardService();
    }

    public CreditCardValidationResult validate(CreditCardValidationRequest request) {
        logger.debug("Validating Credit card:" + request.number);
        CreditCardValidationResult result = new CreditCardValidationResult();
        result.number = request.number;
        try {
            CreditCardNumber creditCardNumber = new CreditCardNumber(request.number);
            CreditCard creditCard = new CreditCard(creditCardNumber,
                    YearMonth.parse(request.date, creditCardDateFormatter));

            // Credit Card type validation is avoided in the constructor of the `CreditCardClass` intentionally
            // This allows scalability of application to support different types.
            // If not this check can be added to the CreditCardClass constructor
            if (creditCard.getType() == CreditCardType.OTHERS) {
                throw throwInvalidCreditCardType();
            }

            if (blackListedCreditCardService.isBlackListed(creditCard.getNumber())) {
                throw CreditCardBlackListedException.throwInvalidCreditCardNumber();
            }

            result.validationResult = new ValidationResult();
            result.validationResult.errorCode = 200;
            result.validationResult.reason = VALID_CREDIT_CARD_MESSAGE;

        } catch (InvalidCreditCardNumberFormatException |
                InvalidCreditCardNumberException |
                CreditCardOutdatedException |
                CreditCardNotSupportedException |
                DateTimeParseException |
                CreditCardBlackListedException e) {
            result.validationResult = buildResultFromKnownExceptions(e);
        }

        logger.debug("Created validation Result for" + request.number);
        return result;
    }

    private ValidationResult buildResultFromKnownExceptions(Exception exception) {
        ValidationResult validationResult = new ValidationResult();
        if (exception instanceof InvalidCreditCardNumberFormatException) {
            logger.debug("Credit Card has invalid format");
            validationResult.errorCode = 421;
        }

        if (exception instanceof InvalidCreditCardNumberException) {
            logger.debug("Credit Card number failed validation check");
            validationResult.errorCode = 422;
        }

        if (exception instanceof CreditCardOutdatedException) {
            logger.debug("Credit Card date is outdated");
            validationResult.errorCode = 423;
        }

        if (exception instanceof CreditCardNotSupportedException) {
            logger.debug("Credit Card  is not VISA or MASTER CARD");
            validationResult.errorCode = 424;
        }

        if (exception instanceof CreditCardBlackListedException) {
            logger.debug("Credit Card is blacklisted");
            validationResult.errorCode = 425;
        }

        if (exception instanceof DateTimeParseException) {
            logger.debug("Invalid Date format");
            validationResult.errorCode = 426;
        }

        validationResult.reason = exception.getMessage();

        return validationResult;
    }
}
