package com.nitin.service;

class CreditCardBlackListedException extends Exception {
    public static String CREDIT_CARD_BLACK_LISTED = "Credit Card Black listed";

    private CreditCardBlackListedException(String message) {
        super(message);
    }

    static CreditCardBlackListedException throwInvalidCreditCardNumber() {
        return new CreditCardBlackListedException(CREDIT_CARD_BLACK_LISTED);
    }
}
