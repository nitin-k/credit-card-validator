package com.nitin.api.dto;

public class CreditCardValidationResult {
    public String number;
    public ValidationResult validationResult;
}
