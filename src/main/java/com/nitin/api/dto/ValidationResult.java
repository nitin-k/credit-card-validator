package com.nitin.api.dto;

/**
 * the Following are assumptions for error codes
 * Error codes are useful for maintaining purpose. As the consumer need not rely on the text
 * 200: 'Credit card valid'
 * 421: 'Invalid credit card number format'
 * 422: 'Failed the credit card Luhn check validation'
 * 423: 'Credit Card Date outdated'
 * 424: 'If the credit is not either of VISA or MASTERCARD'
 * 425: 'Credit Blacklisted'
 * 426: 'Invalid Date Format'
 */
public class ValidationResult {
    public int errorCode;
    public String reason;
}
