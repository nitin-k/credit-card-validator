package com.nitin.api.dto;

public class CreditCardValidationRequest {
    public String number;
    public String date;

    public CreditCardValidationRequest(String number, String date) {
        this.number = number;
        this.date = date;
    }
}
