package com.nitin.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class GenericExceptionHandler implements ExceptionMapper<Throwable> {

    private static final Logger logger = LoggerFactory.getLogger(GenericExceptionHandler.class);

    @Override
    public Response toResponse(Throwable exception) {
        logger.error("Error processing request:", exception);
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }
}
