package com.nitin.api.resources;

import com.nitin.api.dto.CreditCardValidationRequest;
import com.nitin.api.dto.CreditCardValidationResult;
import com.nitin.service.CreditCardValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path("/credit-cards-validate")
public class ValidateCreditCard {
    private static final Logger logger = LoggerFactory.getLogger(ValidateCreditCard.class);


    /**
     * the Following are assumptions for error codes
     * Error codes are useful for maintaining purpose. As the consumer need not rely on the text
     * 200: 'Credit card valid'
     * 421: 'Invalid credit card number format'
     * 422: 'Failed the credit card Luhn check validation'
     * 423: 'Credit Card Date outdated'
     * 424: 'If the credit is not either of VISA or MASTERCARD'
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response validateCreditCards(List<CreditCardValidationRequest> creditCardJsons) throws Exception {
        logger.debug("Received request for validation credit cards");
        CreditCardValidationService creditCardValidationService = new CreditCardValidationService();
        List<CreditCardValidationResult> results = creditCardJsons.stream().map
                (creditCardValidationService::validate).collect(Collectors.toList());

        return Response.status(Response.Status.OK).entity(results).build();
    }

}
