package com.nitin.domain;

public class InvalidCreditCardNumberException extends Exception {
    public static String LUHN_CHECK_FAILED = "Number failed the Luhn Check";

    private InvalidCreditCardNumberException(String message) {
        super(message);
    }

    public static InvalidCreditCardNumberException throwInvalidCreditCardNumber() {
        return new InvalidCreditCardNumberException(LUHN_CHECK_FAILED);
    }
}
