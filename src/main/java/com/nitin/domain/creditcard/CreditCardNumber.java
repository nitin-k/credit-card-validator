package com.nitin.domain.creditcard;

import com.google.common.base.CharMatcher;
import com.nitin.domain.InvalidCreditCardNumberException;
import com.nitin.domain.InvalidCreditCardNumberFormatException;

import static com.nitin.domain.InvalidCreditCardNumberException.throwInvalidCreditCardNumber;
import static com.nitin.domain.InvalidCreditCardNumberFormatException.throwInValidCreditCardNumberFormat;

public class CreditCardNumber {
    private String number;

    public CreditCardNumber(String number) throws InvalidCreditCardNumberFormatException, InvalidCreditCardNumberException {
        if (validateNumberFormat(number)) {
            String stringWithOutSpaces = CharMatcher.whitespace().removeFrom(number);
            if (LuhnCheckService.check(stringWithOutSpaces)) {
                this.number = stringWithOutSpaces;
            } else {
                throw throwInvalidCreditCardNumber();
            }
        }
    }

    private boolean validateNumberFormat(String number) throws InvalidCreditCardNumberFormatException {
        String pattern = "\\d{16}|(\\d{4}\\s\\d{4}\\s\\d{4}\\s\\d{4})";
        if (!number.matches(pattern)) {
            throw throwInValidCreditCardNumberFormat();
        }

        return true;
    }

    public String getNumber() {
        return number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CreditCardNumber)) {
            return false;
        }

        CreditCardNumber that = (CreditCardNumber) o;

        return number != null ? number.equals(that.number) : that.number == null;
    }

    @Override
    public int hashCode() {
        return number != null ? number.hashCode() : 0;
    }
}
