package com.nitin.domain.creditcard;

public enum CreditCardType {
    VISA,
    MASTERCARD,
    OTHERS
}
