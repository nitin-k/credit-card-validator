package com.nitin.domain.creditcard;

import com.google.common.primitives.Ints;
import com.nitin.domain.CreditCardOutdatedException;

import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Locale;

import static com.nitin.domain.CreditCardOutdatedException.throwInvalidCreditCardDate;

public class CreditCard {

    public static DateTimeFormatter creditCardDateFormatter = new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .appendPattern("MM/yy")
            .toFormatter(Locale.ENGLISH);

    private CreditCardNumber number;
    private YearMonth expirationDate;

    public CreditCard(CreditCardNumber number, YearMonth expirationDate) throws
            CreditCardOutdatedException {

        this.number = number;
        if (checkForValidDate(expirationDate)) {
            this.expirationDate = expirationDate;
        }
    }

    public CreditCardType getType() {
        if (number.getNumber().startsWith("4")) {
            return CreditCardType.VISA;
        }
        Integer firstTwoNumber = Ints.tryParse(number.getNumber().substring(0, 2));
        if (firstTwoNumber != null && (firstTwoNumber >= 51 && firstTwoNumber <= 55)) {
            return CreditCardType.MASTERCARD;
        }

        return CreditCardType.OTHERS;
    }

    private boolean checkForValidDate(YearMonth expirationDate) throws CreditCardOutdatedException {
        if (expirationDate.compareTo(YearMonth.now()) < 0) {
            throw throwInvalidCreditCardDate();
        }

        return true;
    }

    public CreditCardNumber getNumber() {
        return number;
    }
}
