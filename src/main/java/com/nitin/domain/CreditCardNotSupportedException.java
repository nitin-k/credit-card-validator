package com.nitin.domain;

public class CreditCardNotSupportedException extends Exception {

    private static String INVALID_CREDIT_CARD_TYPE = "Only VISA/MASTER CARD supported";

    private CreditCardNotSupportedException() {
        super(INVALID_CREDIT_CARD_TYPE);
    }

    public static CreditCardNotSupportedException throwInvalidCreditCardType() {
        return new CreditCardNotSupportedException();
    }
}
