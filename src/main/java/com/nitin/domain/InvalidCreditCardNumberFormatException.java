package com.nitin.domain;

public class InvalidCreditCardNumberFormatException extends Exception {
    public static String FORMAT_NOT_ALLOWED = "Invalid Credit Card Format";

    private InvalidCreditCardNumberFormatException(String message) {
        super(message);
    }

    public static InvalidCreditCardNumberFormatException throwInValidCreditCardNumberFormat() {
        return new InvalidCreditCardNumberFormatException(FORMAT_NOT_ALLOWED);
    }
}
