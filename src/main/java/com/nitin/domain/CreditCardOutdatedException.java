package com.nitin.domain;

public class CreditCardOutdatedException extends Exception {
    public static String INVALID_EXPIRATION_DATE = "Credit Card date is expired";

    private CreditCardOutdatedException() {
        super(INVALID_EXPIRATION_DATE);
    }

    public static CreditCardOutdatedException throwInvalidCreditCardDate() {
        return new CreditCardOutdatedException();
    }
}
