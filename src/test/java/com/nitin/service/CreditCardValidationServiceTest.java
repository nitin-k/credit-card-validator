package com.nitin.service;

import com.nitin.api.dto.CreditCardValidationRequest;
import com.nitin.api.dto.CreditCardValidationResult;
import org.junit.Test;

import static com.nitin.domain.CreditCardOutdatedException.INVALID_EXPIRATION_DATE;
import static com.nitin.domain.InvalidCreditCardNumberException.LUHN_CHECK_FAILED;
import static com.nitin.domain.InvalidCreditCardNumberFormatException.FORMAT_NOT_ALLOWED;
import static com.nitin.service.CreditCardBlackListedException.CREDIT_CARD_BLACK_LISTED;
import static com.nitin.service.CreditCardValidationService.VALID_CREDIT_CARD_MESSAGE;
import static org.assertj.core.api.Assertions.assertThat;

public class CreditCardValidationServiceTest {
    @Test
    public void validate() throws Exception {
        CreditCardValidationService creditCardValidationService = new CreditCardValidationService();

        CreditCardValidationRequest validVisa = new CreditCardValidationRequest("4118578487382760", "10/20");
        CreditCardValidationResult validVisaResult = creditCardValidationService.validate(validVisa);
        assertThat(validVisaResult.validationResult.errorCode).isEqualTo(200);
        assertThat(validVisaResult.validationResult.reason).isEqualTo(VALID_CREDIT_CARD_MESSAGE);

        CreditCardValidationRequest validMastercard = new CreditCardValidationRequest("5164827523445762", "12/20");
        CreditCardValidationResult validMasterCardResult = creditCardValidationService.validate(validMastercard);
        assertThat(validMasterCardResult.validationResult.errorCode).isEqualTo(200);
        assertThat(validMasterCardResult.validationResult.reason).isEqualTo(VALID_CREDIT_CARD_MESSAGE);

        CreditCardValidationRequest luhnCheckFailed = new CreditCardValidationRequest("4118578487382761", "10/20");
        CreditCardValidationResult luhnCheckFailedResult = creditCardValidationService.validate(luhnCheckFailed);
        assertThat(luhnCheckFailedResult.validationResult.errorCode).isEqualTo(422);
        assertThat(luhnCheckFailedResult.validationResult.reason).isEqualTo(LUHN_CHECK_FAILED);

        CreditCardValidationRequest moreThan16Digits = new CreditCardValidationRequest("411857848738276022", "10/20");
        CreditCardValidationResult moreThan16DigitsResult = creditCardValidationService.validate(moreThan16Digits);
        assertThat(moreThan16DigitsResult.validationResult.errorCode).isEqualTo(421);
        assertThat(moreThan16DigitsResult.validationResult.reason).isEqualTo(FORMAT_NOT_ALLOWED);

        CreditCardValidationRequest validFormat = new CreditCardValidationRequest("4118 5784 8738 2760", "10/20");
        CreditCardValidationResult validFormatResult = creditCardValidationService.validate(validFormat);
        assertThat(validFormatResult.validationResult.errorCode).isEqualTo(200);
        assertThat(validFormatResult.validationResult.reason).isEqualTo(VALID_CREDIT_CARD_MESSAGE);

        CreditCardValidationRequest invalidFormat = new CreditCardValidationRequest("41185 7848 7382760", "10/19");
        CreditCardValidationResult invalidFormatResult = creditCardValidationService.validate(invalidFormat);
        assertThat(invalidFormatResult.validationResult.errorCode).isEqualTo(421);
        assertThat(invalidFormatResult.validationResult.reason).isEqualTo(FORMAT_NOT_ALLOWED);

        CreditCardValidationRequest pastDatedCard = new CreditCardValidationRequest("4118578487382760", "10/17");
        CreditCardValidationResult pastDatedResult = creditCardValidationService.validate(pastDatedCard);
        assertThat(pastDatedResult.validationResult.errorCode).isEqualTo(423);
        assertThat(pastDatedResult.validationResult.reason).isEqualTo(INVALID_EXPIRATION_DATE);

        CreditCardValidationRequest invalidDateFormat = new CreditCardValidationRequest("4118578487382760", "19/19");
        CreditCardValidationResult invalidDateFormatResult
                = creditCardValidationService.validate(invalidDateFormat);
        assertThat(invalidDateFormatResult.validationResult.errorCode).isEqualTo(426);

        CreditCardValidationRequest creditCardBlacklisted =
                new CreditCardValidationRequest("4798 1176 5878 1703", "10/20");
        CreditCardValidationResult creditCardBlacklistedResult
                = creditCardValidationService.validate(creditCardBlacklisted);
        assertThat(creditCardBlacklistedResult.validationResult.errorCode).isEqualTo(425);
        assertThat(creditCardBlacklistedResult.validationResult.reason).isEqualTo(CREDIT_CARD_BLACK_LISTED);

        CreditCardValidationRequest invalidCreditCardNumber =
                new CreditCardValidationRequest("aaaa bbbb cccc dddd", "10/19");
        CreditCardValidationResult invalidNumberResult = creditCardValidationService.validate(invalidCreditCardNumber);
        assertThat(invalidNumberResult.validationResult.errorCode).isEqualTo(421);
        assertThat(invalidNumberResult.validationResult.reason).isEqualTo(FORMAT_NOT_ALLOWED);

        CreditCardValidationRequest emptyCreditCardNumber =
                new CreditCardValidationRequest("", "10/19");
        CreditCardValidationResult emptyCreditCardNumberResult = creditCardValidationService.validate(emptyCreditCardNumber);
        assertThat(emptyCreditCardNumberResult.validationResult.errorCode).isEqualTo(421);
        assertThat(emptyCreditCardNumberResult.validationResult.reason).isEqualTo(FORMAT_NOT_ALLOWED);
    }
}
