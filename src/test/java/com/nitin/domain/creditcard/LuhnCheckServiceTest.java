package com.nitin.domain.creditcard;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class LuhnCheckServiceTest {
    @Test
    public void check() throws Exception {
        LuhnCheckService luhnCheckService = new LuhnCheckService();
        assertThat(luhnCheckService.check("4118578487382760")).isTrue();
        assertThat(LuhnCheckService.check("4723199590895148")).isTrue();
        assertThat(luhnCheckService.check("4723199590895149")).isFalse();
        assertThat(luhnCheckService.check("4723199590895149")).isFalse();
        assertThat(luhnCheckService.check("5164827523445762")).isTrue();
        assertThat(luhnCheckService.check("5164827523445761")).isFalse();
        assertThat(luhnCheckService.check("4234567890123456")).isTrue();
    }
}
