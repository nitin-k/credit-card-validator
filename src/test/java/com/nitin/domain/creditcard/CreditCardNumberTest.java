package com.nitin.domain.creditcard;

import com.nitin.domain.InvalidCreditCardNumberException;
import com.nitin.domain.InvalidCreditCardNumberFormatException;
import org.junit.Test;

import static com.nitin.domain.InvalidCreditCardNumberFormatException.FORMAT_NOT_ALLOWED;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.Java6Assertions.assertThat;

public class CreditCardNumberTest {

    @Test
    public void testCardNumberFormatValidation() throws Exception {
        String validCreditCardNumber = "4234567890123456";
        assertThatCode(() -> new CreditCardNumber( validCreditCardNumber)).doesNotThrowAnyException();

        String validCreditCardNumberWithSpaces = "4234 5678 9012 3456";
        assertThatCode(() -> new CreditCardNumber(validCreditCardNumberWithSpaces)).doesNotThrowAnyException();

        String creditCardNumberWithUnEvenSpaces = "42345 6789 01234 56";
        Throwable creditCardNumberWithSpace =
                catchThrowable(() -> new CreditCardNumber(creditCardNumberWithUnEvenSpaces));
        assertThat(creditCardNumberWithSpace)
                .isInstanceOf(InvalidCreditCardNumberFormatException.class)
                .hasMessage(FORMAT_NOT_ALLOWED);

        String greaterThan16Digits = "4234567890123456444";
        Throwable invalidCreditCardLengthException =
                catchThrowable(() ->
                        new CreditCardNumber(greaterThan16Digits));
        assertThat(invalidCreditCardLengthException).isInstanceOf(InvalidCreditCardNumberFormatException.class).hasMessage
                (InvalidCreditCardNumberFormatException.FORMAT_NOT_ALLOWED);

        String hasAlfaNumericString = "aa34567890123456";
        Throwable invalidCreditCardNumber =
                catchThrowable(() ->
                        new CreditCardNumber(hasAlfaNumericString));
        assertThat(invalidCreditCardNumber).isInstanceOf(InvalidCreditCardNumberFormatException.class).hasMessage
                (InvalidCreditCardNumberFormatException.FORMAT_NOT_ALLOWED);
    }

    @Test
    public void testCardNumberLuhnCheckServiceTest() {
        String validCreditCardNumber = "4234567890123456";
        assertThatCode(() -> new CreditCardNumber(validCreditCardNumber)).doesNotThrowAnyException();

        String luhnCheckFailedCreditCard = "4723199590895149";
        Throwable invalidCreditCardLengthException =
                catchThrowable(() ->
                        new CreditCardNumber(luhnCheckFailedCreditCard));
        assertThat(invalidCreditCardLengthException).isInstanceOf(InvalidCreditCardNumberException.class).hasMessage
                (InvalidCreditCardNumberException.LUHN_CHECK_FAILED);
    }
}
