package com.nitin.domain.creditcard;

import com.nitin.domain.CreditCardOutdatedException;
import org.junit.Test;

import java.time.YearMonth;

import static com.nitin.domain.creditcard.CreditCard.creditCardDateFormatter;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.Java6Assertions.assertThat;

public class CreditCardTest {

    @Test
    public void testCardTypeValidation() throws Exception {
        String validVisaNumber = "4234567890123456";
        CreditCard visaCard = new CreditCard(new CreditCardNumber(validVisaNumber), YearMonth.now());
        assertThat(visaCard.getType()).isEqualTo(CreditCardType.VISA);

        String validMastercardNumber = "5164827523445762";
        CreditCard masterCard = new CreditCard(new CreditCardNumber(validMastercardNumber), YearMonth.now());
        assertThat(masterCard.getType()).isEqualTo(CreditCardType.MASTERCARD);

        String validVisaCreditCardNumber = "6011563936736751";
        CreditCard creditCard = new CreditCard(new CreditCardNumber(validVisaCreditCardNumber), YearMonth.now());
        assertThat(creditCard.getType()).isEqualTo(CreditCardType.OTHERS);
    }

    @Test
    public void testExpirationDateValidation() {

        String validVisaCreditCardNumber = "4234567890123456";
        assertThatCode(() ->
                new CreditCard(
                        new CreditCardNumber(validVisaCreditCardNumber),
                        YearMonth.parse("01/19", creditCardDateFormatter)))
                .doesNotThrowAnyException();

        assertThatCode(() ->
                new CreditCard(
                        new CreditCardNumber(validVisaCreditCardNumber),
                        YearMonth.now()))
                .doesNotThrowAnyException();

        Throwable invalidDatePastMonthsException =
                catchThrowable(() ->
                        new CreditCard(
                                new CreditCardNumber(validVisaCreditCardNumber),
                                YearMonth.now().minusMonths(2)));
        assertThat(invalidDatePastMonthsException).isInstanceOf(CreditCardOutdatedException.class).hasMessage
                (CreditCardOutdatedException.INVALID_EXPIRATION_DATE);

        Throwable invalidDatePastYearsException =
                catchThrowable(() ->
                        new CreditCard(
                                new CreditCardNumber(validVisaCreditCardNumber),
                                YearMonth.now().minusYears(2)));
        assertThat(invalidDatePastYearsException).isInstanceOf(CreditCardOutdatedException.class).hasMessage
                (CreditCardOutdatedException.INVALID_EXPIRATION_DATE);

    }
}
